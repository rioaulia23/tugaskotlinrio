package com.example.tugaskotlin

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (Konstanta.getgenre().equals("Man", true)) {
            et.text = Konstanta.getusername()
            et.setTextColor(Color.GREEN)
        } else {
            et.text = Konstanta.getusername()
            et.setTextColor(Color.rgb(255, 0, 102))
        }



        btnout.setOnClickListener {
            val intent = Intent(this@MainActivity, Login::class.java)

            startActivity(intent)
        }
    }
}
