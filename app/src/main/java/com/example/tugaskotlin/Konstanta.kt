package com.example.tugaskotlin

class Konstanta {
    companion object {
        var username = "tes"
        fun setusername(username: String) {
            this.username = username
        }

        fun getusername(): String {
            return username
        }

        var genre = "pria"
        fun setgenre(genre: String) {
            this.genre = genre
        }

        fun getgenre(): String {
            return genre
        }
    }
}