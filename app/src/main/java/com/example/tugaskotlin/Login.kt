package com.example.tugaskotlin

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.login.*

class Login : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login)
        btn.setOnClickListener {
            val username = user.text.toString()
            if (username.isNotEmpty()) {
                Konstanta.setusername(user.text.toString())
                Konstanta.setgenre(spin.selectedItem.toString())
                Toast.makeText(this, "sukses", Toast.LENGTH_SHORT).show()
                val intent = Intent(this, MainActivity::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(this, "Isi username", Toast.LENGTH_SHORT).show()
            }
        }
    }
}
